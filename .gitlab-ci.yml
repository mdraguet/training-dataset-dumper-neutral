variables:
  # we can use the cern DOCKER_CACHE to avoid overloading dockerhub
  # see here: https://clouddocs.web.cern.ch/containers/tutorials/registry.html#dockerhub-cache
  #
  BUILD_IMAGE: gitlab-registry.cern.ch/atlas/athena/analysisbase:22.2.110
  ATHANALYSIS_IMAGE: gitlab-registry.cern.ch/atlas/athena/athanalysis:22.2.110
  ATHENA_IMAGE: registry.cern.ch/docker.io/atlas/centos7-atlasos-dev:latest-gcc11
  DATA_URL: https://dguest-ci.web.cern.ch/dguest-ci/ci/ftag/dumper

# set the default to the AnalysisBase image
image: $BUILD_IMAGE

## Various steps to be followed by the build
stages:
  - build
  - run
  - cleanup
  - imagebuild
  - docs
  - pages

## Common setup for all jobs
before_script:
  - set +e
  - source ~/release_setup.sh
  - if [[ -d build ]]; then source build/**/setup.sh; fi

## Compile with AnalysisBase
compile_analysisbase:
  stage: build
  script:
    - mkdir -p build && cd build
    - cmake .. | tee cmake.log
    - make clean # make sure we don't have residual compilation results
    - make -j $(nproc) 2>&1 | tee -a cmake.log # dump the log files
  artifacts:
    paths:
      - build

## Compile with AthAnalysis
compile_athanalysis:
  extends: compile_analysisbase
  image: $ATHANALYSIS_IMAGE

## Compile with Athena
compile_athena:
  before_script:
    - set +e
    - source setup/athena.sh
    - if [[ -d build ]]; then source build/**/setup.sh; fi
  extends: compile_analysisbase
  image: $ATHENA_IMAGE

###################################
## run tests
###################################

# AnalysisBase
test_pflow:
  stage: run
  needs: [compile_analysisbase]
  script:
    - test-dumper -d pflow_reduced pflow
  artifacts:
    paths:
      - pflow_reduced
    expire_in: 1 hour

test_pflow_full:
  stage: run
  needs: [compile_analysisbase]
  script:
    - test-dumper -p -d pflow_full pflow
  artifacts:
    paths:
      - pflow_full
    expire_in: 1 hour

test_slim:
  stage: run
  needs: [compile_analysisbase]
  script:
    - test-dumper slim

test_truth:
  stage: run
  needs: [compile_analysisbase]
  script:
    - test-dumper -d $PWD/ci-pflow-truth truth
  artifacts:
    paths:
      - ci-pflow-truth
    expire_in: 1 hour

test_truthjets:
  stage: run
  needs: [compile_analysisbase]
  script:
    - test-dumper truthjets

test_trackjets:
  stage: run
  needs: [compile_analysisbase]
  script:
    - test-dumper -d $PWD/ci-trackjets trackjets
  artifacts:
    paths:
      - ci-trackjets
    expire_in: 1 hour

test_fatjets:
  stage: run
  needs: [compile_analysisbase]
  script:
    - test-dumper fatjets

test_trackless:
  stage: run
  needs: [compile_analysisbase]
  script:
    - test-dumper trackless

test_data:
  stage: run
  needs: [compile_analysisbase]
  script:
    - test-dumper data

# AthAnalysis
.run_athanalysis: &run-athanalysis
  image: $ATHANALYSIS_IMAGE
  stage: run
  needs: [compile_athanalysis]

test_ca:
  <<: *run-athanalysis
  script:
    - test-dumper ca

test_ca_minimal:
  <<: *run-athanalysis
  script:
    - test-dumper minimal

test_reduced_ca:
  <<: *run-athanalysis
  script:
    - test-dumper -r ca

test_multi_ca:
  <<: *run-athanalysis
  script:
    - test-dumper multi

test_ca_flow:
  <<: *run-athanalysis
  script:
    - test-dumper flow

test_ca_trigger_emtopo:
  <<: *run-athanalysis
  script:
    - test-dumper -d trig_reduced trigger-emtopo
  artifacts:
    paths:
      - trig_reduced
    expire_in: 1 hour

test_ca_trigger_emtopo_full:
  <<: *run-athanalysis
  script:
    - test-dumper -p -d trig_full trigger-emtopo
  artifacts:
    paths:
      - trig_full
    expire_in: 1 hour

test_ca_softe:
  <<: *run-athanalysis
  script:
    - test-dumper softe

# Athena
.run_athena: &run-athena
  image: $ATHENA_IMAGE
  stage: run
  needs: [compile_athena]
  before_script:
    - set +e
    - source setup/athena.sh
    - if [[ -d build ]]; then source build/**/setup.sh; fi

test_ca_retag:
  <<: *run-athena
  script:
    - test-dumper retag

test_ca_trigger:
  <<: *run-athena
  script:
    - test-dumper trigger

test_ca_trigger_all:
  <<: *run-athena
  script:
    - test-dumper trigger-all

test_ca_trigger_mc16:
  <<: *run-athena
  script:
    - test-dumper trigger-mc16

test_upgrade:
  <<: *run-athena
  script:
    - test-dumper upgrade

test_ca_trigger_trackjet:
  <<: *run-athena
  script:
    - test-dumper trigger-trackjet

test_ca_trigger_fatjet:
  <<: *run-athena
  script:
    - test-dumper trigger-fatjet

# various unit tests
test_singlebtag_submit:
  stage: run
  needs: [compile_analysisbase]
  script:
    - export USER=ci-test
    - source BTagTrainingPreprocessing/grid/setup.sh dry-run
    - grid-submit -d -f single-btag

test_configs:
  stage: run
  needs: [compile_analysisbase]
  script:
    - test-configs-single-b -v

###################################
## compare outputs
###################################

compare_precision_pflow:
  stage: run
  needs: [test_pflow_full, test_pflow]
  script:
    - h5diff -p 0.0015 pflow_full/output.h5 pflow_reduced/output.h5

compare_precision_trigger:
  stage: run
  needs: [test_ca_trigger_emtopo_full, test_ca_trigger_emtopo]
  script: h5diff -p 0.0005 trig_full/output.h5 trig_reduced/output.h5

###################################
## build images
###################################
build_img_latest:
  needs:
    - compile_analysisbase
  stage: imagebuild
  allow_failure: true
  image: gitlab-registry.cern.ch/ci-tools/docker-image-builder:no_kaniko
  variables:
    GIT_SUBMODULE_STRATEGY: recursive
    FROM: $BUILD_IMAGE
    TO: $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_SLUG
  tags:
    - docker-image-build
  script:
    - ignore

build_img_tag:
  needs:
    - compile_analysisbase
  stage: imagebuild
  allow_failure: true
  image: gitlab-registry.cern.ch/ci-tools/docker-image-builder:no_kaniko
  variables:
    GIT_SUBMODULE_STRATEGY: recursive
    FROM: $BUILD_IMAGE
    TO: $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_SLUG
  tags:
    - docker-image-build
  script:
    - ignore
  only:
    - tags

###################################
## build documentation
###################################
docs_pflow:
  stage: docs
  needs: [test_truth]
  script:
    - docs/scripts/ci-dump-variable-docs pflow ci-pflow-truth/output.h5
  artifacts:
    paths:
      - ci-docs-pflow
    expire_in: 1 hour

docs_trackjets:
  stage: docs
  needs: [test_trackjets]
  script:
    - docs/scripts/ci-dump-variable-docs trackjets ci-trackjets/output.h5
  artifacts:
    paths:
      - ci-docs-trackjets
    expire_in: 1 hour

pages:
  image: python:3.11
  stage: pages
  needs: [docs_trackjets, docs_pflow]
  before_script:
    - "" # overwrite default, do nothing
  script:
    - docs/scripts/ci-mkdocs
  artifacts:
    paths:
      - public
    expire_in: 1 hour
