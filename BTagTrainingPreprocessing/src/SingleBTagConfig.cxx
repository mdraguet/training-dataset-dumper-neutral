#include "SingleBTagConfig.hh"
#include "ConfigFileTools.hh"

#define BOOST_BIND_GLOBAL_PLACEHOLDERS // ignore deprecated ptree issues
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>
#include <boost/optional/optional.hpp>

#include <filesystem>
#include <set>

// set up some nlohmann converters from json, used by nlohmann::get<T>()
NLOHMANN_JSON_SERIALIZE_ENUM( DL2Config::Where, {
    {DL2Config::Where::UNKNOWN, ""},
    {DL2Config::Where::JET, "jet"},
    {DL2Config::Where::BTAG, "btag"},
})
NLOHMANN_JSON_SERIALIZE_ENUM( DL2Config::Engine, {
    {DL2Config::Engine::UNKNOWN, ""},
    {DL2Config::Engine::DL2, "dl2"},
    {DL2Config::Engine::GNN, "gnn"},
    {DL2Config::Engine::XBB, "xbb"},
})

NLOHMANN_JSON_SERIALIZE_ENUM( JetLinkWriterConfig::Type, {
    {JetLinkWriterConfig::Type::UNKNOWN, ""},
    {JetLinkWriterConfig::Type::FlowElement, "flow"},
    {JetLinkWriterConfig::Type::IParticle, "iparticle"},
})

namespace {

  const std::string g_ip_prefix = "ip_prefix";

  TrackSortOrder get_track_sort_order(const boost::property_tree::ptree& pt,
                                const std::string& key) {
    std::string val = pt.get<std::string>(key);
    if (val == "abs_d0_significance") {
      return TrackSortOrder::ABS_D0_SIGNIFICANCE;
    }
    if (val == "abs_d0") {
      return TrackSortOrder::ABS_D0;
    }
    if (val == "d0_significance") {
      return TrackSortOrder::D0_SIGNIFICANCE;
    }
    if (val == "abs_beamspot_d0") {
      return TrackSortOrder::ABS_BEAMSPOT_D0;
    }
    if (val == "pt") {
      return TrackSortOrder::PT;
    }
    throw std::logic_error("sort order '" + val + "' not recognized");
  }

  std::vector<JetConstituentWriterConfig::Output> get_constituent_variables(
    const boost::property_tree::ptree& pt)
  {
    using Type = JetConstituentWriterConfig::Type;
    using Op = JetConstituentWriterConfig::Output;
    namespace cf = ConfigFileTools;
    std::vector<JetConstituentWriterConfig::Output> vars;
    std::set<std::string> keys = cf::get_key_set(pt);
    auto fill = [&vars, &keys, &pt](const std::string& name, Type type) {
      if (keys.erase(name)) {
        for (auto v: cf::get_list(pt.get_child(name))) {
          vars.emplace_back<Op>({type, v});
        }
      }
    };
    fill("uchars", Type::UCHAR);
    fill("chars", Type::CHAR);
    fill("ints", Type::INT);
    fill("halves", Type::HALF);
    fill("floats", Type::FLOAT);
    fill("customs", Type::CUSTOM);
    fill("half_precision_customs", Type::CUSTOM_HALF);
    cf::throw_if_any_keys_left(keys, "track variable types");
    return vars;
  }

  auto get_constituent_outblock_config(const boost::property_tree::ptree& pt)
  {
    const std::string edm_block_name = "edm_names";
    JetConstituentWriterConfig::OutputBlock cfg;
    cfg.outputs = get_constituent_variables(pt.get_child("variables"));
    if (pt.count(edm_block_name)) {
      for (const auto& [outname, edmname]: pt.get_child("edm_names")) {
        cfg.edm_name[outname] = edmname.data();
      }
    }
    cfg.allow_unused_edm_names = false;
    if (auto v = pt.get_optional<bool>("allow_unused_edm_names")) {
      cfg.allow_unused_edm_names = *v;
    }
    return cfg;
  }

  auto get_constituent_config(const boost::property_tree::ptree& pt) {
    const std::string association_block = "associations";
    JetConstituentWriterConfig writer;
    writer.constituent = get_constituent_outblock_config(pt);
    writer.name = pt.get<std::string>("output_name");
    writer.size = pt.get<size_t>("n_to_save");
    if (pt.count(association_block)) {
      for (const auto& [type, block]: pt.get_child(association_block)) {
        writer.associated[type] = get_constituent_outblock_config(block);
      }
    }
    return writer;
  }

  auto get_jet_link_config(const nlohmann::ordered_json& nlocfg) {
    auto pt = ConfigFileTools::from_nlohmann(nlocfg);
    JetLinkWriterConfig cfg;
    using Type = JetLinkWriterConfig::Type;
    auto type = nlocfg.at("type").get<Type>();
    if (type == Type::UNKNOWN) {
      throw std::runtime_error("unknown flow constituents type");
    }
    cfg.constituents = get_constituent_config(pt);
    cfg.accessor = "constituentLinks";
    cfg.type = type;
    return cfg;
  }

  TrackConfig get_track_config(const nlohmann::ordered_json& nlocfg) {
    namespace cft = ConfigFileTools;
    auto pt = cft::from_nlohmann(nlocfg);
    TrackConfig cfg;

    cfg.sort_order = get_track_sort_order(pt,"sort_order");

    const boost::property_tree::ptree& tracksel = pt.get_child("selection");
    TrackSelectorConfig::Cuts& cuts = cfg.selection.cuts;
    cuts.pt_minimum        = cft::null_as_nan(tracksel,"pt_minimum");
    cuts.abs_eta_maximum   = cft::null_as_nan(tracksel,"abs_eta_maximum");
    cuts.d0_maximum        = cft::null_as_nan(tracksel,"d0_maximum");
    cuts.z0_maximum        = cft::null_as_nan(tracksel,"z0_maximum");
    cuts.si_hits_minimum   = tracksel.get<int>("si_hits_minimum");
    cuts.si_shared_maximum = tracksel.get<int>("si_shared_maximum");
    cuts.si_holes_maximum  = tracksel.get<int>("si_holes_maximum");
    cuts.pix_holes_maximum = tracksel.get<int>("pix_holes_maximum");

    cfg.selection.btagging_link = pt.get<std::string>("btagging_link");

    cfg.writer = get_constituent_config(pt);
    cfg.input_name = pt.get<std::string>("input_name");

    // use the same prefix for selection and everything else
    std::string ip_prefix = pt.get<std::string>(g_ip_prefix);
    if (cfg.writer.constituent.edm_name.count(g_ip_prefix)) {
      throw std::runtime_error(
        "you should not specify '" + g_ip_prefix
        + "' in track edm remapping");
    }
    cfg.writer.constituent.edm_name[g_ip_prefix] = ip_prefix;
    cfg.selection.ip_prefix = ip_prefix;

    return cfg;
  }

  SoftElectronConfig get_electron_config(const nlohmann::ordered_json& nlocfg) {
    namespace cft = ConfigFileTools;
    auto pt = cft::from_nlohmann(nlocfg);
    SoftElectronConfig cfg;
    const boost::property_tree::ptree& elsel = pt.get_child("selection");
    SoftElectronSelectorConfig::Cuts& cuts = cfg.selection.cuts;
    cuts.pt_minimum        = cft::null_as_nan(elsel,"pt_minimum");
    cuts.pt_maximum        = cft::null_as_nan(elsel,"pt_maximum");
    cuts.abs_eta_maximum   = cft::null_as_nan(elsel,"abs_eta_maximum");
    cuts.d0_maximum        = cft::null_as_nan(elsel,"d0_maximum");
    cuts.eta_maximum       = cft::null_as_nan(elsel,"eta_maximum");
    cuts.ptrel_maximum     = cft::null_as_nan(elsel,"ptrel_maximum");
    cuts.isopt_maximum     = cft::null_as_nan(elsel,"isopt_maximum");
    cuts.eop_maximum       = cft::null_as_nan(elsel,"eop_maximum");
    cuts.rhad1_maximum     = cft::null_as_nan(elsel,"rhad1_maximum");
    cuts.wstot_maximum     = cft::null_as_nan(elsel,"wstot_maximum");
    cuts.rphi_maximum      = cft::null_as_nan(elsel,"rphi_maximum");
    cuts.reta_maximum      = cft::null_as_nan(elsel,"reta_maximum");
    cuts.deta1_maximum     = cft::null_as_nan(elsel,"deta1_maximum");
    cuts.dpop_maximum      = cft::null_as_nan(elsel,"dpop_maximum");

    JetLinkWriterConfig writer;
    writer.constituents = get_constituent_config(pt);
    writer.type = JetLinkWriterConfig::Type::Electron;
    writer.accessor = "jet_electrons";
    cfg.writer = writer;

    return cfg;
  }

  HitConfig get_hit_config(const nlohmann::ordered_json& nlocfg) {
    namespace cft = ConfigFileTools;
    auto pt = cft::from_nlohmann(nlocfg);
    HitWriterConfig writer;

    writer.output_size = pt.get<size_t>("output_size");
    writer.name = pt.get<std::string>("output_name");
    writer.dR_to_jet = pt.get<float>("dR_to_jet");
    writer.save_endcap_hits = pt.get<bool>("save_endcap_hits");
    writer.save_only_clean_hits = pt.get<bool>("save_only_clean_hits");
    writer.save_sct = pt.get<bool>("save_sct");

    HitDecoratorConfig decorator;
    decorator.dR_hit_to_jet = writer.dR_to_jet;
    decorator.use_splitProbability = pt.get<bool>("use_splitProbability");
    decorator.save_endcap_hits = writer.save_endcap_hits;

    return {writer, decorator};
  }

  TrackSortOrder get_truth_sort_order(const std::string& val) {
    if (val == "pt") {
      return TrackSortOrder::PT;
    }
    throw std::logic_error("sort order '" + val + "' not recognized");
  }

  TruthSelectorConfig::Particle get_truth_particle(const std::string& name)
  {
    using p = TruthSelectorConfig::Particle;
    if (name == "truth_hadrons") return p::hadron;
    if (name == "truth_leptons") return p::lepton;
    if (name == "truth_fromBC") return p::fromBC;
#define TRY(KEY) if (name == #KEY) return p::KEY
    TRY(overlapLepton);
    TRY(promptLepton);
    TRY(nonPromptLepton);
    TRY(muon);
    TRY(stableNonGeant);
    TRY(higgs);
    TRY(top);
#undef TRY
    throw std::logic_error("unknown truth particle type: " + name);
  }

  TruthConfig get_truth_config(const nlohmann::ordered_json& js) {
    TruthConfig cfg;

    using Opts = ConfigFileTools::OptionalConfigurationObject;
    Opts json(js);

    // the configuration can have either a selection object or a merge
    // list, not both
    Opts association(json.get("association"));
    const nlohmann::ordered_json& merge = json.get("merge");
    if (!association.empty() && !merge.empty()) {
      throw std::runtime_error(
        "Truth config can't contain both association and merging");
    }

    std::string particles = "";
    if (!association.empty()) {
      TruthSelectorConfig tsc;
      particles = association.require<std::string>("particles");
      tsc.particle = get_truth_particle(particles);
      const auto& containers = association.get<std::vector<std::string>>("containers", {});
      if (!containers.empty()) {
        tsc.containers = containers;
      } else {
        tsc.containers = {association.get("container", "TruthParticles")};
      }
      auto& kin = tsc.kinematics;
      kin.pt_minimum = association.get("pt_minimum", 0.0f);
      kin.abs_eta_maximum = association.get("abs_eta_maximum", INFINITY);
      kin.dr_maximum = association.require<float>("dr_maximum");
      cfg.selection = tsc;
    }
    association.throw_if_unused("selection");

    if (!merge.empty()) {
      cfg.merge = merge.get<std::vector<std::string>>();
    }

    cfg.overlap_dr = json.get("overlap_dr", 0.0);

    // use association name if specified, otherwise reuse particles name
    std::string association_name = json.get("association_name", "");
    if (not association_name.empty()) {
      cfg.association_name = association_name;
    }
    else if (not particles.empty()) {
      cfg.association_name = particles;
    }
    else {
      throw std::runtime_error(
        "If not specifying \"association_name\" you need to specify some"
        " \"particles\" to associate");
    }

    Opts output(json.get("output"));
    if (!output.empty()) {
      TruthOutputConfig out;
      out.n_to_save = output.require<int>("n_to_save");
      out.sort_order = get_truth_sort_order(
        output.require<std::string>("sort_order"));
      out.name = output.get("name", cfg.association_name);
      cfg.output = out;
    }
    output.throw_if_unused("output");

    cfg.decorate = json.get("decorate_summary", false);

    json.throw_if_unused("truth config");

    return cfg;
  }

  DecorateConfig get_decoration_config(const nlohmann::ordered_json& raw) {
    namespace cft = ConfigFileTools;
    DecorateConfig cfg;

    // the decorators will default to false, but we also make sure
    // that every decorator specified in the configuration is used.
    ConfigFileTools::OptionalConfigurationObject nlocfg(raw);
#define FILL(field) cfg.field = nlocfg.get(#field, false)
    FILL(jet_aug);
    FILL(btag_jes);
    FILL(soft_muon);
    FILL(track_truth_info);
    FILL(track_sv_info);
    FILL(track_lepton_id);
    FILL(do_vrtrackjets_fix);
    FILL(do_heavyions);
    FILL(lepton_decay_label);
    FILL(truth_pileup);
#undef FILL
    nlocfg.throw_if_unused("decorations");
    return cfg;
  }

  JetCalibrationConfig get_jet_calibration(
    const nlohmann::ordered_json& nlocfg)
  {
    auto pt = ConfigFileTools::from_nlohmann(nlocfg);
    JetCalibrationConfig config;
#define FILL(field) config.field = pt.get<std::string>(#field)
    FILL(collection);
    FILL(configuration);
    FILL(seq);
    FILL(area);
#undef FILL
    return config;
  }

  SelectionConfig get_selection_config(const nlohmann::ordered_json& raw) {
    SelectionConfig cfg;
    ConfigFileTools::OptionalConfigurationObject nlocfg(raw);
    // make sure the type of `defval` matches the type of `field`
#define FILL(field, defval) cfg.field = nlocfg.get(#field, defval)
    FILL(truth_jet_matching, false);
    FILL(truth_jet_collection, "AntiKt4TruthDressedWZJets");
    FILL(truth_primary_vertex_matching, false);
    FILL(minimum_jet_constituents, 0);
    FILL(minimum_jet_pt, 0.0);
    FILL(maximum_jet_pt, INFINITY);
    FILL(maximum_jet_absolute_eta, INFINITY);
    FILL(minimum_jet_mass, 0.0);
    FILL(maximum_jet_mass, INFINITY);
    FILL(minimum_jvt, -INFINITY);
#undef FILL
    // jet cleaning
    std::string jet_cleaning = nlocfg.get("jet_cleaning","none");
    const std::map<std::string, JetCleanOption> jetclean_option_map = {
      {"none", JetCleanOption::none},
      {"event", JetCleanOption::event},
      {"jet", JetCleanOption::jet} };
    if (!jetclean_option_map.count(jet_cleaning)) {
      std::string problem = "unknown jet cleaning '" + jet_cleaning +
        "', pick one of the following";
      std::string sep = ": ";
      for (const auto& itr: jetclean_option_map) {
        problem.append(sep + "'" + itr.first + "'");
        sep = ", ";
      }
      throw std::runtime_error(problem);
    }
    cfg.jet_cleaning = jetclean_option_map.at(jet_cleaning);

    nlocfg.throw_if_unused("selection options");
    return cfg;
  }

  SubjetConfig get_subjet_config(const nlohmann::ordered_json& nlocfg) {
    namespace cft = ConfigFileTools;
    auto subjet = cft::from_nlohmann(nlocfg);
    SubjetConfig cfg;
    cfg.input_name = subjet.get<std::string>("input_name");
    cfg.output_name = subjet.get<std::string>("output_name");
    cfg.n_subjets_to_save = subjet.get<size_t>("n_subjets_to_save");
    cfg.min_jet_pt = subjet.get<double>("min_jet_pt");
    cfg.max_abs_eta = subjet.get<double>("max_abs_eta");
    cfg.num_const = subjet.get<size_t>("num_const");
    cfg.btagging_link = subjet.get<std::string>("btagging_link");
    cfg.tracks_name = subjet.get<std::string>("tracks_name");
    // read in the b-tagging variables
    cfg.variables = cft::get_variable_list(subjet.get_child("variables"));
    return cfg;
  }

  // do some consistency checks
  void requireBTag(bool variable, const std::string& varname,
                   const std::string& btagging_link) {
    if (variable && btagging_link.empty()) {
      throw std::runtime_error(varname + " requires btagging_link to be set");
    }
  }
  void checkConsistency(const SingleBTagConfig& cfg) {
    const std::string& btagname = cfg.btagging_link;
    const DecorateConfig& dec = cfg.decorate;
#define REQUIRES_BTAG(name) requireBTag(dec.name, #name, btagname)
    REQUIRES_BTAG(btag_jes);
    REQUIRES_BTAG(soft_muon);
    REQUIRES_BTAG(jet_aug);
    #undef REQUIRES_BTAG
    const auto& dl2s = cfg.dl2_configs;
    int n_btags = std::count_if(
      dl2s.begin(), dl2s.end(),
      [](auto& x) { return x.where == DL2Config::Where::BTAG; });
    requireBTag(n_btags > 0, "applying btags", btagname);

    if (cfg.vertex_collection.empty()) {
      if (cfg.selection.truth_primary_vertex_matching) {
        throw std::runtime_error(
          "truth primary vertex matching requires primary vertex");
      }
      if (cfg.hits) {
        throw std::runtime_error("hits variables require primary vertex");
      }
    }
  }

  DL2Config get_dl2_config(const nlohmann::ordered_json& node) {

    // keeps track of used keys
    ConfigFileTools::OptionalConfigurationObject nlocfg(node);

    using Where = DL2Config::Where;
    auto where = nlocfg.get<Where>("where", Where::BTAG);
    if (where == Where::UNKNOWN) {
      throw std::runtime_error("unknown tagging target");
    }

    using Engine = DL2Config::Engine;
    auto engine = nlocfg.get<Engine>("engine", Engine::DL2);
    if (engine == Engine::UNKNOWN) throw std::runtime_error(
      "unknown tagging engine");

    // get the variable remapping
    const auto& remap_obj = nlocfg.get("remapping", nlohmann::json::object());
    const auto& remapping = remap_obj.get<std::map<std::string, std::string>>();

    // get the file path
    std::string nn_file_path = nlocfg.get<std::string>("nn_file_path", "");
    if (nn_file_path.empty()) throw std::runtime_error("No nn_file_path found in DL2 config");

    // get the flip tag config
    const auto& flip_tag_config = FlavorTagDiscriminants::flipTagConfigFromString(
      nlocfg.get("flip_tag_config", "STANDARD")
    );

    // check we used everything and return
    nlocfg.throw_if_unused("DL2 key");
    return {where, engine, nn_file_path, remapping, flip_tag_config};
  }

}

const nlohmann::ordered_json get_merged_json(const std::filesystem::path& cfg_path)
{
  namespace fs = std::filesystem;
  namespace cft = ConfigFileTools;
  if (!fs::exists(cfg_path)) {
    throw std::runtime_error(cfg_path.string() + " doesn't exist");
  }
  std::ifstream cfg_stream(cfg_path);
  auto nlocfg = nlohmann::ordered_json::parse(cfg_stream);
  cft::combine_files(nlocfg, cfg_path.parent_path());
  return nlocfg;
}

SingleBTagConfig get_singlebtag_config(const std::filesystem::path& cfg_path)
{
  auto nlocfg = get_merged_json(cfg_path);
  return get_singlebtag_config(nlocfg, cfg_path.stem());
}

SingleBTagConfig get_singlebtag_config(const nlohmann::ordered_json& nlocfg,
                                       const std::string& tool_prefix)
{
  namespace pt = boost::property_tree;
  namespace cft = ConfigFileTools;
  using OJ = nlohmann::ordered_json;
  using VOJ = std::vector<OJ>;

  SingleBTagConfig config;
  config.tool_prefix = tool_prefix;

  ConfigFileTools::OptionalConfigurationObject opt_nlocfg(nlocfg);

  config.selection = get_selection_config(opt_nlocfg.get("selection"));

  config.jet_collection = opt_nlocfg.require<std::string>("jet_collection");
  config.is21p9_AOD = opt_nlocfg.get("is21p9_AOD", false);

  const auto& jet_calib = opt_nlocfg.require<OJ>("calibration");
  if (!jet_calib.empty()) {
    config.calibration = get_jet_calibration(jet_calib);
  }

  if (config.selection.jet_cleaning == JetCleanOption::jet &&
      config.jet_collection == "AntiKt4EMPFlowJets") {
    throw std::runtime_error(
      "Must not use individual jet cleaning for " + config.jet_collection +
      ". Use {\"jet_cleaning\": \"event\"} instead.");
  }

  config.vertex_collection = opt_nlocfg.get("vertex_collection", "");
  config.btagging_link = opt_nlocfg.get("btagging_link","");

  for (const auto& trkpt: opt_nlocfg.require<VOJ>("tracks")) {
    config.tracks.push_back(get_track_config(trkpt));
  }

  // electrons config
  const auto& electrons = opt_nlocfg.get("electrons");
  if (!electrons.empty()) {
    config.electrons = get_electron_config(electrons);
  }

  config.nntc = opt_nlocfg.get<std::string>("nntc","");

  // The hits block should exist for trackless studies
  const auto& hits = opt_nlocfg.get("hits");
  if (!hits.empty()) {
    config.hits = get_hit_config(hits);
  }

  for (const auto& trthpt: opt_nlocfg.get<VOJ>("truths", {})) {
    config.truths.push_back(get_truth_config(trthpt));
  }

  for (const auto& nn_cfg: opt_nlocfg.get<VOJ>("dl2_configs", {})) {
    config.dl2_configs.push_back(get_dl2_config(nn_cfg));
  }

  // subjet config
  for (const auto& subjet: opt_nlocfg.get<VOJ>("subjets", {})) {
    config.subjet_configs.push_back(get_subjet_config(subjet));
  }

  // pflow constituents
  const auto& flow = opt_nlocfg.get("flow");
  if (!flow.empty()) {
    config.flow = get_jet_link_config(flow);
  }

  const auto& nlo_variables = opt_nlocfg.require<OJ>("variables");
  auto variables = cft::from_nlohmann(nlo_variables);
  config.btag = cft::get_variable_list(variables.get_child("btag"));
  config.default_flag_mapping = cft::check_map_from(
    cft::get_variable_list(variables.get_child("default_mapping")));

  config.decorate = get_decoration_config(opt_nlocfg.get("decorate"));

  opt_nlocfg.throw_if_unused("top level options");

  checkConsistency(config);

  return config;
}

void force_full_precision(JetConstituentWriterConfig::OutputBlock& block) {
  using Type = JetConstituentWriterConfig::Type;
  for (auto& output: block.outputs) {
    if (output.type == Type::HALF) output.type = Type::FLOAT;
    if (output.type == Type::CUSTOM_HALF) output.type = Type::CUSTOM;
  }
}

void force_full_precision(SingleBTagConfig& cfg) {
  cfg.force_full_precision = true;
  for (auto& trk: cfg.tracks) {
    force_full_precision(trk.writer.constituent);
    for (auto& [_, block]: trk.writer.associated) {
      force_full_precision(block);
    }
  }
}
