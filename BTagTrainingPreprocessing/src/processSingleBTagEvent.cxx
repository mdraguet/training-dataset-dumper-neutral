#include "processSingleBTagEvent.hh"

// the implementation is the same for every signature, we just have to
// recompile it a few times below. We do it here to avoid having the
// template headers interfere with anything that calls this function.

#include "SingleBTagTools.hh"
#include "SingleBTagConfig.hh"
#include "BJetShallowCopier.hh"
#include "JetTruthAssociator.hh"
#include "TruthTools.hh"
#include "cleanHits.hh"
#include "TrackLeptonDecorator.hh"

#include "JetWriters/IJetLinkWriter.h"

#include "FlavorTagDiscriminants/BTagMuonAugmenter.h"
#include "JetMomentTools/JetVertexTaggerTool.h"
#include "xAODRootAccess/TEvent.h"
#include "xAODTruth/TruthParticleContainer.h"
#include "xAODTruth/TruthEventContainer.h"
#include "xAODTruth/TruthPileupEventContainer.h"
#include "xAODTruth/TruthVertexContainer.h"
#include "xAODJet/JetContainer.h"
#include "xAODTracking/TrackParticleContainer.h"
#include "xAODTracking/TrackMeasurementValidationContainer.h"

#include "xAODMuon/MuonContainer.h"
#include "xAODEgamma/ElectronContainer.h"

#include "xAODHIEvent/HIEventShapeAuxContainer.h"
#include "xAODHIEvent/HIEventShapeContainer.h"

#include "PathResolver/PathResolver.h"

namespace {

  void check_rc(StatusCode code) {
    if (!code.isSuccess()) throw std::runtime_error("bad return code");
  }

  constexpr float operator "" _GeV(unsigned long long d) { return d*1e3; }
  constexpr float operator "" _GeV(long double d) { return d*1e3; }

  const xAOD::Vertex* primary(const xAOD::VertexContainer& vertices) {
    if (vertices.size() == 0) {
      throw std::runtime_error("no primary vertices");
    }
    for ( const xAOD::Vertex *vertex : vertices ) {
      if ( vertex->vertexType() == xAOD::VxType::PriVtx ) {
        return vertex;
      }
    }
    // if we find nothing else this should be the beam spot
    return vertices.front();
  }
}

// this is the templated code, the concrete instances are below
template <typename Event>
void processSingleBTagEventImpl(Event& event,
                                const SingleBTagConfig& jobcfg,
                                const SingleBTagTools& tools,
                                SingleBTagOutputs& out) {
  const xAOD::EventInfo *event_info = nullptr;
  check_rc( event.retrieve(event_info, "EventInfo") );

  // jet event cleaning: do not process events with bad jets
  if (jobcfg.selection.jet_cleaning == JetCleanOption::event) {
    bool result = tools.acc.eventClean_looseBad(*event_info);
    // do not process event, if event-level jet cleaning flag is not passed
    if (!result) return;
  }

  // save some primary vertex information on eventinfo
  const xAOD::Vertex* pv = nullptr;
  if (!jobcfg.vertex_collection.empty()) {
    const xAOD::VertexContainer* primary_vertices = nullptr;
    check_rc( event.retrieve(primary_vertices, jobcfg.vertex_collection));
    pv = primary(*primary_vertices);

    tools.dec.n_primary_vertices(*event_info) = primary_vertices->size();
    tools.dec.primary_vertex_detector_z(*event_info) = pv->z();
    tools.dec.primary_vertex_detector_z_uncertainty(*event_info) = (
      std::sqrt(pv->covariancePosition()(2,2)));
  }

  const xAOD::TruthVertex* truth_PV = nullptr;
  if ( jobcfg.decorate.track_truth_info ||
       jobcfg.selection.truth_primary_vertex_matching ) {

    if(jobcfg.is21p9_AOD){
      const xAOD::TruthEventContainer* truthEventContainer = nullptr;
      check_rc( event.retrieve(truthEventContainer, "TruthEvents"));
      // truthEventContainer always has size == 1?
      truth_PV = truthEventContainer->at(0)->truthVertex(0);
    }
    else{
      const xAOD::TruthVertexContainer* truthPVContainer = nullptr;
      check_rc( event.retrieve(truthPVContainer, "TruthPrimaryVertices"));
      truth_PV = truthPVContainer->at(0);
    }

    if(truth_PV == nullptr) throw std::runtime_error("no truth primary vertex");

  }
  if (jobcfg.decorate.truth_pileup) {
    const xAOD::TruthPileupEventContainer* pileupEventContainer = nullptr;
    check_rc( event.retrieve(pileupEventContainer, "TruthPileupEvents"));
    tools.dec.n_pileup_events(*event_info) = pileupEventContainer->size();
  }

  // Reject events with PV badly reconstructed
  if (jobcfg.selection.truth_primary_vertex_matching) {
    if (std::abs( pv->z() - truth_PV->z() ) > 0.1) return;
  }

  if (jobcfg.decorate.do_heavyions) {
    //retrieve FCal ET for collisions centrality percentiles. do_heavyions(true) is needed.
    const xAOD::HIEventShapeContainer* calos = 0;
    check_rc( event.retrieve(calos, "CaloSums") );
    tools.dec.fcal_et_tev(*event_info) = calos->at(5)->et() * 1e-6; // MeV->TeV
  }

  // read the jets
  const xAOD::JetContainer *raw_jets = nullptr;
  check_rc( event.retrieve(raw_jets, jobcfg.jet_collection) );

  // shallow copy the jets
  auto [jets, aux] = tools.shallow_copier.shallowCopyBJets(*raw_jets);

  //get the pixel hits and SCT hits from the AOD
  std::vector<const xAOD::TrackMeasurementValidation*> hits;
  if (jobcfg.hits) {
    hits = getHits(event, *jobcfg.hits, tools);
  }

  // decorate the jet with links to selected particles from one or more containers
  for (const auto& truth_associator: tools.jet_truth_associators) {
    std::vector<const xAOD::TruthParticleContainer*> tpcs;
    for (const std::string& container_name: truth_associator.containers) {
      const xAOD::TruthParticleContainer* tpc = nullptr;
      check_rc( event.retrieve(tpc, container_name) );
      tpcs.push_back(tpc);
    }
    for (const xAOD::Jet* uncalib_jet: *jets) {
      truth_associator.decorate(*uncalib_jet, &tpcs);
    }
  }

  // first jet loop: add decorations to jets. These have to be done
  // before calibration to be consistent with reconstruction
  for (const xAOD::Jet* uncalib_jet: *jets) {

    // get the b-tagging link
    const xAOD::BTagging* btag = tools.acc.btaggingLink(*uncalib_jet);

    // do some basic decorations
    if (jobcfg.decorate.jet_aug) {
      tools.jet_augmenter.augment(*btag, *btag);
    }
    if (tools.muon_augmenter) {
      tools.muon_augmenter->augment(*btag);
    }
    if (jobcfg.decorate.btag_jes){
      tools.jet_augmenter.augmentBtagJes(*btag, *btag);
    }

    // run taggers on b-tagging object or jet
    for (const auto& dl2: tools.dl2s) dl2(*btag);
    for (const auto& nn: tools.jet_nns) nn(*uncalib_jet);

    // merge existing truth decorations on the jet
    for (const auto& merger: tools.jet_truth_mergers) {
      merger.decorate(*uncalib_jet);
    }

    // PG: temporary fix for missing VR track jet decorators
    // Fixed in https://gitlab.cern.ch/atlas/athena/-/merge_requests/49966
    if (jobcfg.decorate.do_vrtrackjets_fix && (jets->size() == 1)) {
      tools.dec.trackjet_rel_dR(*uncalib_jet) = INFINITY;
      tools.dec.trackjet_abs_dR(*uncalib_jet) = INFINITY;
    }

  } // end first loop over jets

  // apply jet calibration
  if (jobcfg.calibration) {
    check_rc(tools.calibration_tool.applyCalibration(*jets));
  }

  // sort jets by descending pt
  // we make a new container first to preserve the indices
  std::vector<const xAOD::Jet*> sorted_jets(jets->begin(), jets->end());
  std::sort(sorted_jets.begin(), sorted_jets.end(),
            [](const auto* j1, const auto* j2) {
              return j1->pt() > j2->pt();
            });

  // decorate jet with hit information
  if (jobcfg.hits) {
    for (const xAOD::Jet* jet: sorted_jets) {
      tools.hit_decorator->decorate(*jet, hits, *pv);
    }
  }

  // decorate lepton decay label
  if (jobcfg.decorate.lepton_decay_label){
    for (const xAOD::Jet* jet: sorted_jets) {
      tools.jet_lepton_decay_label_decorator.decorate(*jet);
    }
  }

  // decorate tracks with reco lepton ID
  if (tools.trkLeptonDecorator) {
    const xAOD::TrackParticleContainer* tracks = nullptr;
    const xAOD::MuonContainer* muons = nullptr;
    const xAOD::ElectronContainer* electrons = nullptr;
    check_rc(event.retrieve(tracks, "InDetTrackParticles"));
    check_rc(event.retrieve(muons, "Muons"));
    check_rc(event.retrieve(electrons, "Electrons"));
    tools.trkLeptonDecorator->decorate(*tracks, *muons, *electrons);
  }

  // Retrieve AntiKt4TruthJets collection if we are doing truth jet matching
  const xAOD::JetContainer *truth_jets = nullptr;
  if (jobcfg.selection.truth_jet_matching) {
    check_rc( event.retrieve(truth_jets, jobcfg.selection.truth_jet_collection));
  }

  // second jet loop: select calibrated jets and write out to HDF5
  unsigned int rank = 0;
  std::vector<const xAOD::Jet*> jets_to_write;
  const SelectionConfig& sel = jobcfg.selection;
  for (const xAOD::Jet* calib_jet : sorted_jets) {

    // overlap removal
    const auto& vetos = tools.overlap_checks;
    auto veto_check = [&j=*calib_jet](auto& f) {return f(j); };
    if (std::any_of(vetos.begin(), vetos.end(), veto_check)) continue;

    // don't bother using JVT if it's set to -inf
    if (tools.jvttool) {
      float jvt = NAN;
      if (jobcfg.calibration) {
        // if we're calibrating jets we need to check the JVT again
        jvt = tools.jvttool->updateJvt(*calib_jet);
      } else {
        jvt = tools.acc.jvt(*calib_jet);
      }
      bool fail_jvt = (
        calib_jet->pt() > 20_GeV &&
        calib_jet->pt() < 60_GeV &&
        std::abs(calib_jet->eta()) < 2.4 &&
        jvt < sel.minimum_jvt );
      if (fail_jvt) continue;
      tools.dec.jvt(*calib_jet) = jvt;
    }

    // only do jet-level jet cleaning if not doing event-level jet cleaning
    if (sel.jet_cleaning == JetCleanOption::jet) {
      if (!tools.cleaning_tool.keep(*calib_jet)) continue;
    }

    // kinematic requirements
    if (calib_jet->pt() < sel.minimum_jet_pt or calib_jet->pt() > sel.maximum_jet_pt) continue;
    if (std::abs(calib_jet->eta()) > sel.maximum_jet_absolute_eta) continue;
    if (calib_jet->m() < sel.minimum_jet_mass or calib_jet->m() > sel.maximum_jet_mass) continue;

    // minimum constituent requirement
    if (calib_jet->numConstituents() < sel.minimum_jet_constituents) continue;

    // truth jet matching
    if (sel.truth_jet_matching) {
      if (!truth::passed_truth_jet_matching(*calib_jet, *truth_jets)) {
        continue;
      }
    }

    // get the b-tagging object
    const xAOD::BTagging* btag = tools.acc.btaggingLink(*calib_jet);

    // decorate jet with lepton MCTC truth info
    if (jobcfg.decorate.soft_muon) {
      tools.lepTruthDecorator.decorate(*btag, *calib_jet);
    }

    // decorate jet with summary info about associated truth collections
    for (const auto& dec: tools.jetTruthSummaryDecorators) {
      dec.decorate(*calib_jet);
    }

    // decorate jet pT rank (jets are already sorted by pT)
    tools.dec.jet_rank(*calib_jet) = rank++;
    // Decorate and write subjets
    for (auto& collection : out.subjets) {
      std::vector<const xAOD::Jet*> subjets = collection.getSubjets(calib_jet);
      // decorate tracks with subjet index
      collection.trkSubjetsDecorator.decorate(calib_jet, subjets);
      collection.jet_writer.write_with_parent(subjets, calib_jet);
      collection.n_subjets(*calib_jet) = subjets.size();
    }

    // write out tracks associated with jets
    for (auto& tracktool: out.tracks ) {

      // get tracks from uncalibrated jets
      const xAOD::Jet* uncalib_jet = raw_jets->at(calib_jet->index());
      auto tracks = tracktool.selector.get_tracks(*uncalib_jet);

      // decorate secondary vertexing info to the tracks
      if ( jobcfg.decorate.track_sv_info ) {
        tools.trkVertexDecorator.decorateAll(tracks, *btag, *pv);
      }

      // run the NN track classifier tool
      if ( !jobcfg.nntc.empty() ) {
        std::vector<const xAOD::TrackParticle*> selected_tracks;
        for (const auto& track: tracks) {
          if ( tools.track_classifier.selectTrack(track, calib_jet) ) {
            selected_tracks.push_back(track);
          }
        }
        tracks = selected_tracks;
      }

      // decorate truth info to tracks
      if ( jobcfg.decorate.track_truth_info ) {
           tools.trkTruthDecorator.decorateAll(tracks);
      }

      // sort tracks and write
      const auto sorted_tracks = tracktool.sorted(tracks, *uncalib_jet);
      tracktool.writer.write(sorted_tracks, *uncalib_jet);
      tracktool.n_tracks_decorator(*calib_jet) = tracks.size();
    }

    // write out electrons associated with jets
    if (out.electrons) {

      // get electrons from uncalibrated jets
      const xAOD::ElectronContainer *electrons = nullptr;
      check_rc(event.retrieve(electrons, "Electrons"));
      
      // select electrons from jet and write them
      const xAOD::Jet* uncalib_jet = raw_jets->at(calib_jet->index());
      out.electrons->select_and_write(uncalib_jet, electrons);
    }

    const xAOD::Jet* uncalib_jet = jets->at(calib_jet->index());

    // write hits
    if (out.hits) {
      out.hits->write(hits, *uncalib_jet, *pv);
    }

    // write truth particles
    for (auto& truthtool: out.truths) {
      truthtool.write(*uncalib_jet);
    }

    // write flow objects
    if (out.flow) out.flow->write(*uncalib_jet);

    // collect jets for output
    jets_to_write.push_back(calib_jet);
  }

  // write out jets
  if (!jets_to_write.empty()) {
    out.jet_writer.write(jets_to_write, event_info);
  }

}

// Concrete versions of the templated function above. These are the
// ones that are exposed to be used in other files.
void processSingleBTagEvent(xAOD::TEvent& e,
                            const SingleBTagConfig& c,
                            const SingleBTagTools& t,
                            SingleBTagOutputs& o) {
  processSingleBTagEventImpl(e, c, t, o);
}

// StoreGateSvc isn't defined in AnalysisBase...
#ifndef XAOD_STANDALONE
void processSingleBTagEvent(StoreGateSvc& e,
                            const SingleBTagConfig& c,
                            const SingleBTagTools& t,
                            SingleBTagOutputs& o) {
  processSingleBTagEventImpl(e, c, t, o);
}
#else
// ... but SgTEvent is
void processSingleBTagEvent(asg::SgTEvent& e,
                            const SingleBTagConfig& c,
                            const SingleBTagTools& t,
                            SingleBTagOutputs& o) {
  processSingleBTagEventImpl(e, c, t, o);
}
#endif  // XAOD_STANDALONE
